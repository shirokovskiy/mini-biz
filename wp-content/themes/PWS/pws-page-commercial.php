<?php /* Template Name: PWS-Custom-Page */ ?>
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 04.03.15
 * Time         : 23:13
 * Description  :
 */
if (isset($wp_query->query_vars['jSearch']) && $wp_query->query_vars['jSearch'] == 'search'):
    $wpTitle = 'Коммерческая недвижимость на стадии строительства и ремонта | Свой Бизнес';
endif;

get_header();?>
<div class="col-md-8">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <h1 class="block-title"><span>КОММЕРЧЕСКАЯ НЕДВИЖИМОСТЬ НА СТАДИИ СТРОИТЕЛЬСТВА И РЕМОНТА</span></h1>
            <?php
            if (isset($_POST) && $_POST['miniSearch']) {
                $_SESSION['searchData'] = $_POST;
            }

            // выборка объектов либо расширенная форма поиска
            if (isset($wp_query->query_vars['jSearch']) && $wp_query->query_vars['jSearch'] == 'search'):
                $iTypeId = 1; // коммерческая недвижимость (тип)

                /**
                 * Соединение с базой сайта Петростройбаза
                 */
                $db2 = new wpdb('psbuser','JwJbWsdQ','psbaza','localhost');

                /**
                 * Выборка по региональному признаку
                 */
                $sql = "SELECT sral_sr_id FROM `site_regions_access_lnk` WHERE sral_srat_id = 1"; // 1 = СПб и ЛО и весь СЗ
                $records = $db2->get_results($sql);

                if (is_array($records) && !empty($records)) {
                    foreach($records as $obj) {
                        $arrRegions[] = $obj->sral_sr_id;
                    }
                }

                /**
                 * Пагинатор
                 */
                $r = 30; // записей на страницу
                $p = 1;
                $pages = 1;

                if (isset($wp_query->query_vars['lp']) && $wp_query->query_vars['lp'] > 1) {
                    $p = (int) $wp_query->query_vars['lp']; // get_query_var('p')
                }

                $ps = ($p - 1) * $r;

                /**
                 * Фильтр
                 */
                $sqlWhere = '';
                if (isset($_SESSION['searchData']['estateGoal']) && !empty($_SESSION['searchData']['estateGoal'])) {
                    $sqlWhere .= " AND seo_id_goal = ".intval($_SESSION['searchData']['estateGoal']);
                }

                if (isset($_SESSION['searchData']['estateRegion']) && !empty($_SESSION['searchData']['estateRegion'])) {
                    $arrRegions = array_intersect($arrRegions, $_SESSION['searchData']['estateRegion']);
                }

                if (isset($_SESSION['searchData']['estateDistrict']) && !empty($_SESSION['searchData']['estateDistrict'])) {
                    foreach ($_SESSION['searchData']['estateDistrict'] as $k => $val) {
                        if (empty($val)) {
                            unset($_SESSION['searchData']['estateDistrict'][$k]);
                        }
                    }

                    if (!empty($_SESSION['searchData']['estateDistrict']))
                        $sqlWhere .= " AND sra_id IN (".implode(',',$_SESSION['searchData']['estateDistrict']).")";
                }

                $iSquareFrom = 0;
                if (isset($_SESSION['searchData']['squareFrom']) && !empty($_SESSION['searchData']['squareFrom'])) {
                    $iSquareFrom = (float) $_SESSION['searchData']['squareFrom'];

                    if ($iSquareFrom > 0)
                        $sqlWhere .= " AND seo_sq_total > ".$iSquareFrom;
                }

                $iSquareTo = 0;
                if (isset($_SESSION['searchData']['squareTo']) && !empty($_SESSION['searchData']['squareTo'])) {
                    $iSquareTo = (float) $_SESSION['searchData']['squareTo'];

                    if ($iSquareTo > $iSquareFrom)
                        $sqlWhere .= " AND seo_sq_total < ".$iSquareTo;
                }

                $iFloorFrom = 0;
                if (isset($_SESSION['searchData']['floorFrom']) && !empty($_SESSION['searchData']['floorFrom'])) {
                    $iFloorFrom = (int) $_SESSION['searchData']['floorFrom'];

                    if ($iFloorFrom > 0)
                        $sqlWhere .= " AND seo_etazh > ".$iFloorFrom;
                }

                $iFloorTo = 0;
                if (isset($_SESSION['searchData']['floorTo']) && !empty($_SESSION['searchData']['floorTo'])) {
                    $iFloorTo = (int) $_SESSION['searchData']['floorTo'];

                    if ($iFloorTo > $iFloorFrom)
                        $sqlWhere .= " AND seo_etazh < ".$iFloorTo;
                }

                if (isset($_SESSION['searchData']['dateQuarter']) && !empty($_SESSION['searchData']['dateQuarter'])) {
                    $iDateQuarter = (int) $_SESSION['searchData']['dateQuarter'];

                    $iDateYear = (int) date('Y');
                    if (isset($_SESSION['searchData']['dateYear']) && !empty($_SESSION['searchData']['dateYear'])) {
                        $iDateYear = (int) $_SESSION['searchData']['dateYear'];
                    }

                    switch ($iDateQuarter) {
                        case 1:
                            $sDateStart = $iDateYear.'-01-01';
                            $sDateEnd = $iDateYear.'-03-31';
                            break;
                        case 2:
                            $sDateStart = $iDateYear.'-04-01';
                            $sDateEnd = $iDateYear.'-06-30';
                            break;
                        case 3:
                            $sDateStart = $iDateYear.'-07-01';
                            $sDateEnd = $iDateYear.'-09-30';
                            break;
                        case 4:
                            $sDateStart = $iDateYear.'-10-01';
                            $sDateEnd = $iDateYear.'-12-31';
                            break;
                        default:
                            $sDateStart = $iDateYear.'-01-01';
                            $sDateEnd = $iDateYear.'-03-31';
                            break;
                    }

                    $sqlWhere .= " AND seo_etap_date BETWEEN '".$sDateStart."' AND '".$sDateEnd."'";
                }

                /**
                 * Подсчёт количества найденых объектов недвижимости
                 */
                $sql = "SELECT COUNT(seo_id) iC"
                    . " FROM site_estate_objects"
                    . " WHERE seo_id IN ("
                        . " SELECT tSeo.seo_id"
                        . " FROM site_estate_objects tSeo"
                        . " LEFT JOIN site_images tSi ON (si_id_rel = seo_id)"
                        . " LEFT JOIN site_estate_types tSet ON (seo_id_etype = set_id)"
                        . " LEFT JOIN site_regions_adm tSra ON (seo_id_adm_reg = sra_id)"
                        . " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)"
                        . " LEFT JOIN site_regions tSr ON (sorl_sr_id = sr_id)"
                        . " WHERE seo_status = 'Y' AND seo_sb = 'Y' "
                        . " AND sorl_sr_id IN (".implode(',' , $arrRegions).")".$sqlWhere
                        . " GROUP BY seo_id"
                    . ")";

                $records = $db2->get_col($sql);
                $colRec = 0;
                if (isset($records[0]) && !empty($records[0])) {
                    $colRec = intval($records[0]);
                    $pages = (int) floor( $colRec / $r );
                    $ost = $colRec % $r; // если хвостика нет, то пустую страницу показывать незачем (последняя страница будет пустая)
                    $pages = ($ost > 0 ? $pages + 1 : $pages);
                }

                /**
                 * Выборка объектов недвижимости
                 */
                $sql = "SELECT tSeo.seo_name, tSeo.seo_other_adm_reg, tSeo.seo_url, tSeo.seo_id, tSeo.seo_naznach"
                    . ", tSeo.seo_etap_date, tSeo.seo_address, tSi.si_filename, tSra.sra_name, tSet.set_name, tSr.sr_name"
                    . " FROM site_estate_objects tSeo"
                    . " LEFT JOIN site_images tSi ON (si_id_rel = seo_id)"
                    . " LEFT JOIN site_estate_types tSet ON (seo_id_etype = set_id)"
                    . " LEFT JOIN site_regions_adm tSra ON (seo_id_adm_reg = sra_id)"
                    . " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)"
                    . " LEFT JOIN site_regions tSr ON (sorl_sr_id = sr_id)"
                    . " WHERE seo_status = 'Y' AND seo_sb = 'Y' " //.($_GET['images']==1 ? " AND si_id_rel IS NOT NULL" : '')
                    . " AND sorl_sr_id IN (".implode(',' , $arrRegions).")".$sqlWhere
                    . " GROUP BY seo_id ORDER BY seo_etap_date DESC, seo_date_change DESC, seo_date_add DESC, seo_id DESC LIMIT $ps, $r";
                $records = $db2->get_results($sql); ?>

                <table class="estate-objects">
                <?php
                $iWidth = 165;

                if (is_array($records) && !empty($records)) {
                    foreach($records as $obj) {
                        // Images
                        $isImage = false;
                        $fileImage = 'dummy.jpg';

                        if (file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg')) {
                            $isImage = true;
                        } elseif (!file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg')
                            && file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.b.jpg')) {
                            /**
                             * Случай, когда главная картинка ещё не создана для проекта Свой Бизнес для данного объекта
                             */
                            $image = wp_get_image_editor( THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.b.jpg' );
                            if ( ! is_wp_error( $image ) ) {
                                $image->resize( $iWidth, 110, false /* crop */ );
                                $fileImage = 'objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg';
                                $image->save( THEME_ABSPATH.'/images/'.$fileImage );
                                $isImage = true;
                            }
                        } else {
                            /**
                             * Случай когда берём картинку из Фотобанка
                             */
                            $sql = "SELECT si_id, si_filename, si_id_rel, si_title FROM `site_images` WHERE `si_foto_object_id` = ".$obj->seo_id." AND `si_status` = 'Y' AND `si_type` = 'fotobank'";
                            $fotobankImages = $db2->get_results($sql);

                            if (is_array($fotobankImages) && !empty($fotobankImages)) {
                                foreach($fotobankImages as $oImage) {
                                    if (file_exists(THEME_ABSPATH.'/images/groups/'.$oImage->si_id_rel.'/'.$oImage->si_filename)) {

                                        $fileImage = 'groups/'.$oImage->si_id_rel.'/t_h_u_m_b_'.$oImage->si_filename;
                                        $isImage = true;
                                    }

                                    if ($isImage)
                                        break;
                                }
                            }
                        }

                        # HTML code ?>
                        <tr class="object">
                            <td class="foto"><img src="<?php bloginfo('stylesheet_directory') ?>/images/<?=$fileImage?>" alt="<?=htmlspecialchars($obj->seo_name)?>"/></td>
                            <td class="info">
                                <a href="/object/<?=$obj->seo_url?>"><h4><?=htmlspecialchars($obj->seo_name)?></h4></a>
                                <p><span>ID</span><?=$obj->seo_id?></p>
                                <p><span>Назначение</span><?= htmlspecialchars($obj->set_name) ?></p>
                                <p><span>Регион</span><?= htmlspecialchars($obj->sr_name) ?></p>
                                <p><span>Район</span><?= is_null($obj->sra_name) ? htmlspecialchars($obj->seo_other_adm_reg) : htmlspecialchars($obj->sra_name) ?></p>
                                <p><span>Адрес</span><?= htmlspecialchars($obj->seo_address) ?><!--comment class="map-mark"><a href="#">показать на карте</a></comment--></p>
                                <p><span>Тип недвижимости</span><?= htmlspecialchars($obj->set_name) ?></p>
                            </td>
                        </tr>
<?php               }
                }
                ?>
                </table>

                <p style="text-align: center">Всего найдено: <?php echo $colRec, ' ', semantic($colRec, $arrObjectsCountString) ?>.</p>

                <table class="paginator-box">
                    <tr>
                        <td align="left">
<?php                   if ($pages > 1):
                            $isInteraptor =
                            $isInteraptor2 =
                            $isInterapted =
                            $isInterapted2 = false;

                            $p = ($p == 0 ? 1 : $p);

                            $step_break = 5;
                            $step_break_m = 2;

                            if ($pages > ($step_break_m*2+$step_break)) {
                                $isInteraptor = $isInteraptor2 = true;
                                $hidden_pages = $pages - $step_break_m*2 - $step_break;
                                $hidden_pages_half = floor($hidden_pages/2);

                                if ($p <= $step_break) {
                                    $isInteraptor = false;
                                    $hidden_pages_half = $hidden_pages;
                                }

                                if ($p >= ($pages - $step_break)) {
                                    $isInteraptor2 = false;
                                    $hidden_pages_half = -$hidden_pages_half;
                                }
                            }
                            ?>

                            <table class="paginator">
                                <tr>
                                    <td class="arrows back"><a href="?lp=<?=($p-1)?>" title="Назад">&nbsp;</a></td>
                                    <?php for ($s = 1; $s <= $pages; $s++):
                                        if ($s>$step_break_m && $isInteraptor && $s<($p-2)):
                                            if (!$isInterapted):
                                                $isInterapted = true; ?>
                                                <td style="border: 0">...</td>
                                            <?php endif;
                                            continue;
                                        endif;


                                        if ($s>=($p-2+$step_break) && $isInteraptor2 && $s<=($pages - $step_break_m)):
                                            if (!$isInterapted2):
                                                $isInterapted2 = true; ?>
                                                <td style="border: 0">...</td>
                                            <?php   endif;
                                            continue;
                                        endif ?>
                                        <td><a href="?lp=<?php echo $s ?>"<?php echo $p == $s ? ' class="active"':'' ?>><?=$s?></a></td>
                                    <?php endfor ?>
                                    <td class="arrows fwd"><a href="?lp=<?=($p+1)?>" title="Вперёд">&nbsp;</a></td>
                                </tr>
                            </table>
<?php                   endif; ?>
                        </td>
                        <td style="text-align: right">
                            <a href="/kommercheskaya-nedvizhimost/">Изменить параметры поиска &gt;&gt;</a>
                        </td>
                    </tr>
                </table>

<?php       else: // расширенная форма поиска ?>

                <form action="/kommercheskaya-nedvizhimost/search" method="post"><input type="hidden" value="1" name="miniSearch" />
                    <div id="search-home-box" class="block">
                        <div class="col-md-6">
                            <h4 class="box-title"><span>Назначение недвижимости</span></h4>

                            <?php
                            /**
                             * Соединение с базой сайта Петростройбаза
                             */
                            $db2 = new wpdb('psbuser','JwJbWsdQ','psbaza','localhost');
                            $goals = $db2->get_results("SELECT * FROM `site_estate_goals` ORDER BY `seg_order`");
                            ?>

                            <div class="select-box w2">
                                <select name="estateGoal" id="estateGoal">
                                    <option value="">---</option>
                                    <?php if (is_array($goals) && !empty($goals)): ?>
                                        <?php foreach($goals as $record): ?>
                                            <option value="<?php echo $record->seg_id ?>"<?= ($record->seg_id == $_SESSION['searchData']['estateGoal'] ? ' selected':'')?>><?php echo $record->seg_goal ?></option>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>

                            <h4 class="box-title" style="margin-top: 20px"><span>Цена за м<sup>2</sup></span></h4>
                            <div class="box-params price">
                                <div class="col-md-6" style="padding: 0">
                                    <span>От:</span>
                                    <input type="text" name="priceFrom" maxlength="6" value="<?=$_SESSION['searchData']['priceFrom']?>" disabled />
                                    <span>т.р.</span>
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <span>До:</span>
                                    <input type="text" name="priceTo" maxlength="8" value="<?=$_SESSION['searchData']['priceTo']?>" disabled />
                                    <span>т.р.</span>
                                </div>
                            </div>

                            <h4 class="box-title"><span>Этажность</span></h4>
                            <div class="box-params price">
                                <div class="col-md-6" style="padding: 0">
                                    <span>От:</span>
                                    <input type="text" name="floorFrom" maxlength="6" value="<?=$_SESSION['searchData']['floorFrom']?>" />
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <span>До:</span>
                                    <input type="text" name="floorTo" maxlength="8" value="<?=$_SESSION['searchData']['floorTo']?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="box-title"><span>Общая площадь</span></h4>
                            <div class="box-params square">
                                <div class="col-md-6" style="padding: 0">
                                    <span>От:</span>
                                    <input type="text" name="squareFrom" maxlength="10" value="<?=$_SESSION['searchData']['squareFrom']?>"/>
                                    <span>м<sup>2</sup></span>
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <span>До:</span>
                                    <input type="text" name="squareTo" maxlength="10" value="<?=$_SESSION['searchData']['squareTo']?>"/>
                                    <span>м<sup>2</sup></span>
                                </div>
                            </div>

                            <h4 class="box-title"><span>Срок сдачи</span></h4>
                            <div class="box-params">
                                <div class="col-md-6" style="padding: 0">
                                    <span class="select-label">Квартал:</span>
                                    <?php $iCQuarter = intval(date('m')/3 + 1) ?>
                                    <?php $arrQuarters = array(1=>'I','II','III','IV') ?>
                                    <div class="select-box w4">
                                        <select name="dateQuarter" id="dateQuarter">
                                            <option value=""></option>
                                        <?php foreach($arrQuarters as $num => $val): ?>
                                            <option value="<?=$num?>"<?=($num==$_SESSION['searchData']['dateQuarter']?' selected':'')?>><?=$val?></option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <span class="select-label">Год:</span>
                                    <div class="select-box w3">
                                        <select name="dateYear" id="dateYear">
                                            <option value=""></option>
                                            <?php for($y=2010;$y<(date('Y')+5);$y++):?>
                                            <option value="<?=$y?>"<?=($y==$_SESSION['searchData']['dateYear']?' selected':'')?>><?=$y?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="clear: both"></div>
                        <hr style="width: 96%"/>

                        <div class="col-md-12" id="estateDistrictSelectAll">
                            <h4 class="box-title"><span>Район</span></h4>

                            <div class="checkbox-box" style="margin-top: 10px">
                                <input type="checkbox" name="estateDistrictAll" id="estateDistrictAll" value="all"/>
                                <label for="estateDistrictAll"><span></span>все</label>
                            </div>

                            <div class="select-regions-box">
                                <div class="col-md-6" id="estateDistrictSelectCity">
                                    <div class="checkbox-box" style="margin-top: 10px">
                                        <input type="checkbox" name="estateRegion[]" id="estateDistrictCity" value="1"<?= (in_array(1, $_SESSION['searchData']['estateRegion'])?' checked':'') ?>/>
                                        <label for="estateDistrictCity"><span></span>Санкт-Петербург</label>
                                    </div>

                                    <?php
                                    /**
                                     * Выборка районов города
                                     */
                                    $regions = $db2->get_results("SELECT * FROM `site_regions_adm` WHERE `sra_status` = 'Y' AND `sra_sr_id` = 1 ORDER BY `sra_sr_id`,`sra_name`"); ?>
                                    <?php if (is_array($regions) && !empty($regions)): ?>
                                        <?php foreach($regions as $record): ?>
                                            <div class="checkbox-box" style="margin-top: 4px">
                                                <input type="checkbox" name="estateDistrict[]" id="estateDistrict<?=$record->sra_id?>" value="<?=$record->sra_id?>"<?=(in_array($record->sra_id,$_SESSION['searchData']['estateDistrict'])?' checked':'')?>/>
                                                <label for="estateDistrict<?=$record->sra_id?>"><span></span><?=$record->sra_name?></label>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                                <div class="col-md-6" id="estateDistrictSelectArea">
                                    <div class="checkbox-box" style="margin-top: 10px">
                                        <input type="checkbox" name="estateRegion[]" id="estateDistrictArea" value="2"<?= (in_array(2, $_SESSION['searchData']['estateRegion'])?' checked':'') ?>/>
                                        <label for="estateDistrictArea"><span></span>Лен.область</label>
                                    </div>

                                    <?php
                                    /**
                                     * Выборка районов области
                                     */
                                    $regions = $db2->get_results("SELECT * FROM `site_regions_adm` WHERE `sra_status` = 'Y' AND `sra_sr_id` = 2 ORDER BY `sra_sr_id`,`sra_name`"); ?>
                                    <?php if (is_array($regions) && !empty($regions)): ?>
                                        <?php foreach($regions as $record): ?>
                                            <div class="checkbox-box" style="margin-top: 4px">
                                                <input type="checkbox" name="estateDistrict[]" id="estateDistrict<?=$record->sra_id?>" value="<?=$record->sra_id?>"<?=(in_array($record->sra_id,$_SESSION['searchData']['estateDistrict'])?' checked':'')?>/>
                                                <label for="estateDistrict<?=$record->sra_id?>"><span></span><?=$record->sra_name?></label>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            jQuery(document).ready(function ($) {
                                // выделить всё
                                $('#estateDistrictAll').click(function(){
                                    $('#estateDistrictSelectAll input[type="checkbox"]').prop('checked', $(this).is(':checked'));
                                });

                                // выделить все районы города
                                $('#estateDistrictCity').click(function(){
                                    $('#estateDistrictSelectCity input[type="checkbox"]').prop('checked', $(this).is(':checked'));
                                });

                                // выделить все районы области
                                $('#estateDistrictArea').click(function(){
                                    $('#estateDistrictSelectArea input[type="checkbox"]').prop('checked', $(this).is(':checked'));
                                });
                            });
                        </script>

                        <div style="clear: both"></div>
                        <hr style="width: 96%"/>

                        <div class="col-md-6">
                            <h4 class="box-title"><span>Станция метро</span></h4>
                            <div class="select-box w2">
                                <select name="subway" id="subway" disabled title="Временно в разработке!">
                                    <option value="">--Временно в разработке!--</option>
                                    <?php if (is_array($subways) && !empty($subways)): ?>
                                        <?php foreach($subways as $record): ?>
                                            <option value="<?php echo $record->seg_id ?>"<?= ($record->seg_id == $_SESSION['searchData']['estateGoal'] ? ' selected':'')?>><?php echo $record->seg_goal ?></option>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>

                            <h4 class="box-title"><span>Сортировать по</span></h4>
                            <div class="select-box w2">
                                <select name="sortBy" id="sortBy" disabled title="Временно в разработке!">
                                    <option value="">--Временно в разработке!--</option>

                                </select>
                            </div>

                            <div style="margin-top: 20px; padding-left: 0">
                                <button class="box-button" style="width: 90%">показать</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="box-title"><span>Улица</span></h4>
                            <div class="box-params street">
                                <input type="text" name="street" maxlength="128" value="<?=$_SESSION['searchData']['street']?>"/>
                            </div>
                        </div>
                    </div>
                </form><!-- #search-home-box -->

                <div class="col-md-12">
                    <a href="/kommercheskaya-nedvizhimost/search">Все объекты коммерческой недвижимости</a>
                </div>
<?php       endif; ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        /**
         * Yandex.Maps
         * При клике по ссылке типа
         * <a href="javascript:void(0)" data-address="{strAddressData}" class="link-blue maps" title="Показать на Яндекс.Картах">адрес на карте</a>
         */
        ymaps.ready(function () {
            $('.maps').show().click(function () {
                var address = $(this).attr('data-address');

                if (!isEmpty(address)) {
                    $('#mapBox').show();
                    showEstateObjectMap(address);
                }
            });
        });

        $('#mapBox .close').click(function () {
            $('#map').empty();
            $('#mapBox').hide();
        });
    });
</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
