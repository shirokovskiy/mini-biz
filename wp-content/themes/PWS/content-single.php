<?php
/**
 * @package fabthemes
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	$thumbID = get_post_thumbnail_id();
	$img_url = wp_get_attachment_url( $thumbID,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
	$image = aq_resize( $img_url, 960, 600, true,true,true ); //resize & crop the image
    $thumbnail_images = get_posts(array('p' => $thumbID, 'post_type' => 'attachment'));
	?>
	<?php
    if($image) : ?>
		<img src="<?php echo $image ?>" alt="<?php the_title(); ?>" class="main-article-image" />
    <?php   if ($thumbnail_images && isset($thumbnail_images[0])) {
        echo '<span>'.$thumbnail_images[0]->post_excerpt.'</span>';
            }
	endif; ?>

    <?php
    if (has_shortcode( get_the_content(), 'gallery' )):
        $galleries = get_post_galleries_images();

        $s = 0;

        // Loop through all galleries found
        foreach( $galleries as $gallery ) {
            // Loop through each image in each gallery
            foreach( $gallery as $image ) {
                echo '<a class="highslide post-thumbnail'.($s%6 == 0?' first':'').'" onclick="return hs.expand(this)" href="'.preg_replace("/-[\d]{3}x[\d]{3}/", "", $image).'"><img src="' . $image . '"/></a>';
                $s++;
            }
        }
    endif;
    ?>

    <?php /*if ( get_post_gallery() ) : ?>
        <?= get_post_gallery() ?>
    <?php endif*/ ?>

    <script type="text/javascript">

    </script>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
        $content = get_the_content();
        $content = preg_replace( '/\[gallery.+\]/isU', '', $content );
        $content = apply_filters( 'the_content', $content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        echo $content;
        ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'fabthemes' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php the_tags(); ?>
		<?php fabthemes_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
