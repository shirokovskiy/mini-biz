<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package fabthemes
 */
if (isset($_SESSION['searchData'])) unset($_SESSION['searchData']);
get_header(); ?>
<div class="col-md-12">
	<?php //the_breadcrumb(); ?>
</div>
<div class="col-md-8">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

        <?php
            // get sticky posts from DB
            $sticky = get_option('sticky_posts');
            // check if there are any
            if (!empty($sticky)) {
                // optional: sort the newest IDs first
                rsort($sticky);
                // override the query
                $args = array(
                    'post__in' => $sticky
                    , 'category__in' => array(1) // где 1 - id категории
                );
                query_posts($args);

                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();

                    /* Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part( 'content', get_post_format() );

                endwhile;

                kriesi_pagination();
            } ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

            <h1 class="block-title"><span>коммерческая недвижимость<br />на стадии строительства и ремонта</span></h1>
            <form action="/kommercheskaya-nedvizhimost/search" method="post"><input type="hidden" value="1" name="miniSearch" />
            <div id="search-home-box" class="block">
                <div class="col-md-6">
                    <h4 class="box-title"><span>Назначение недвижимости</span></h4>

                    <?php
                    /**
                     * Соединение с базой сайта Петростройбаза
                     */
                    $db2 = new wpdb('psbuser','JwJbWsdQ','psbaza','localhost');
                    $goals = $db2->get_results("SELECT * FROM `site_estate_goals` ORDER BY `seg_order`");
                    ?>

                    <div class="select-box w2">
                        <select name="estateGoal" id="estateGoal">
                            <option value="">---</option>
                            <?php if (is_array($goals) && !empty($goals)): ?>
                                <?php foreach($goals as $record): ?>
                                    <option value="<?php echo $record->seg_id ?>"><?php echo $record->seg_goal ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>

                    <div class="checkbox-box" style="margin-top: 10px">
                        <input type="checkbox" name="estateRegion[]" id="cbx_spb" value="1" checked/>
                        <label for="cbx_spb"><span></span>Санкт-Петербург</label>
                    </div>
                    <div class="checkbox-box">
                        <input type="checkbox" name="estateRegion[]" id="cbx_lo" value="2"/>
                        <label for="cbx_lo"><span></span>Лен.область</label>
                    </div>


                    <h4 class="box-title"><span>Район</span></h4>

                    <?php
                    /**
                     * Выборка регионов
                     */
                    $regions = $db2->get_results("SELECT * FROM `site_regions_adm` WHERE `sra_status` = 'Y' AND `sra_sr_id` IN (1,2) ORDER BY `sra_sr_id`,`sra_name`");
                    ?>

                    <div class="select-box w2">
                        <select name="estateDistrict[]" id="estateDistrict">
                            <option value="">---</option>
                            <?php if (is_array($regions) && !empty($regions)): ?>
                                <?php foreach($regions as $record): ?>
                                    <option value="<?php echo $record->sra_id ?>"><?php echo ($record->sra_sr_id == 1 ? 'СПб:' : 'ЛО:'),' ', $record->sra_name ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>

                    <div style="margin-top: 20px">
                        <button class="box-button" style="width: 90%">показать</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="box-title"><span>Общая площадь</span></h4>
                    <div class="box-params">
                        <div class="col-md-6" style="padding: 0">
                            <span>От:</span>
                            <input type="text" name="squareFrom" maxlength="10" />
                            <span>м<sup>2</sup></span>
                        </div>
                        <div class="col-md-6" style="padding: 0">
                            <span>До:</span>
                            <input type="text" name="squareTo" maxlength="10"/>
                            <span>м<sup>2</sup></span>
                        </div>
                    </div>

                    <h4 class="box-title"><span>Срок сдачи</span></h4>
                    <div class="box-params">
                        <div class="col-md-6" style="padding: 0">
                            <span class="select-label">Квартал:</span>
                            <?php #$iCQuarter = intval(date('m')/3 + 1) ?>
                            <?php $arrQuarters = array(1=>'I','II','III','IV') ?>
                            <div class="select-box w4">
                                <select name="dateQuarter" id="dateQuarter">
                                    <option value=""></option>
                                    <?php foreach($arrQuarters as $num => $val): ?>
                                    <option value="<?=$num?>"><?=$val?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0">
                            <span class="select-label">Год:</span>
                            <div class="select-box w3">
                                <select name="dateYear" id="dateYear">
                                    <option value=""></option>
                                    <?php for($y = 2010; $y < (date('Y')+5); $y++): ?>
                                    <option value="<?= $y ?>"><?= $y ?></option>
                                    <?php endfor ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form><!-- #search-home-box -->

            <h1 class="block-title"><span>последние события в санкт-петербурге</span></h1>

            <?php
            $args = array(
                'post__in' => $sticky,
                'posts_per_page' => 1 // LIMIT 1
            );

            /**
             * ID категорий
             */
            $anonses = array(
                array(1 => 4, 7)
                , array(1 => 5, 6)
                //, array(1 => 10, 9)
            );

            foreach($anonses as $anons_row): ?>
            <div class="extra-rubrics-box">
                <?php foreach($anons_row as $col => $category_id): ?>
                <div class="col-md-6 col<?=$col?>">
                    <?php
                    wp_reset_query();
                    if ($category_id <= 0) continue;
                    // Get the ID of a given category
                    $category_name = get_cat_name( $category_id );

                    // Get the URL of this category
                    $category_link = get_category_link( $category_id );
                    ?>
                    <h3 class="box-title"><a href="<?php echo esc_url( $category_link ) ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/cat<?php echo $category_id?>.png" alt="<?php echo $category_name ?>" title="<?php echo $category_name ?>"/> <span><?php echo $category_name ?></span></a></h3>
<?php               $args['category__in'] = array($category_id);
                    query_posts($args);

                    if (have_posts()) {
                        while (have_posts()) {
                            the_post(); ?>
                            <p><a href="<?php the_permalink() ?>"><?php the_post_thumbnail(); ?></a></p>
                            <p><a href="<?php the_permalink() ?>"><?php the_title() ?></a></p>
                            <p><?php the_content('<br>Подробнее &gt;&gt;') ?></p>
<?php                   }
                    } ?>
                </div>
                <?php endforeach ?>
            </div>
            <?php endforeach ?>

            <div id="footer-adv-block" class="extra-rubrics-box">
                <div class="col-md-6 col1"><?php drawAdsPlace(array('id'=>1)) ?></div>
                <div class="col-md-6 col2"><?php drawAdsPlace(array('id'=>2)) ?></div>
            </div>

            <?php
            //var_dump(query_posts('cat=5&post_status=publish,future'));
            //var_dump(query_posts('category_name=Спорт'));
            ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
