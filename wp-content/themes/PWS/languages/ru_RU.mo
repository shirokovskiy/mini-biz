��    >        S   �      H  "   I     l     �  0   �     �     �     �  
   �     �       3        G     Z     o     w     �     �  	   �     �     �  _   �  W        o     �  	   �     �  0   �     �  	   �            %   #     I     X     `     g  	   w     �     �  F   �     �     �     �     	     "	     *	  	   >	  \   H	     �	     �	     �	  9   �	     
     
  	   
  &   &
     M
  $   V
  (   {
     �
     �
  X  �
  ,   &  3   S     �  /   �     �     �     �     �     	       �   0  )   �  #   �     �  F        S     o     |     �     �  o   �  �   2  0   �  0   �       0   %  1   V  6   �     �     �  !   �  4     &   E     l     �     �     �     �     �  l   �     \     k  %   �  %   �     �  &   �       �   .  1   �     �       f        �  
   �     �  �   �  
   F  N   Q  =   �     �     �        	       3       %       ;           <            
   ,                    0   >      $   "          &   *          !      =      -               )                           2   8   9   1   (      4                    +   6           :   '      .                  5   7                  /   #       %s <span class="says">says:</span> &larr; Older Comments 1: date, 2: time%1$s at %2$s <span class="meta-nav">&larr;</span> Older posts All Categories Archives Asides Author: %s Category Category title Click OK to reset. Any theme settings will be lost! Comment navigation Comments are closed. Day: %s Default options restored. Edit Footer Galleries Homepage Images It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Maximum height (px) Maximum width (px) Month: %s Newer Comments &rarr; Newer posts <span class="meta-nav">&rarr;</span> News Category Widget No Repeat No file chosen Nothing Found Oops! That page can&rsquo;t be found. Options saved. Page %s Pages: Post navigation Posted by Posts navigation Primary Menu Ready to publish your first post? <a href="%1$s">Get started here</a>. Remove Restore Defaults Save Options Search Results for: %s Sidebar Sidebar Tabs Widget Site Logo Sorry, but nothing matched your search terms. Please try again with some different keywords. Tab widget on the sidebar Theme Options Title: Upgrade your version of WordPress for full media support. Upload Videos View File Widget to display news from categories Year: %s Your comment is awaiting moderation. comments titleOne Comment %1$s Comments post authorby %s post datePosted on %s Project-Id-Version: Endolf v10.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-02-02 21:08+0300
Last-Translator: Игорь <dybabiz@ya.ru>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.3
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../../../../
X-Textdomain-Support: yes
Language: ru
X-Poedit-SearchPath-0: .
 %s <span class="says">говорит:</span> &larr;  Предыдущие комментарии %1$s в %2$s <span class="meta-nav">&larr;</span> Назад Все категории Архивы Заметки Автор: %s Категория  Заголовок Нажмите кнопку OK, чтобы сбросить. Любые настройки темы будут потеряны! Комментарий навигация Обсуждение закрыто День: %s Настройки по умолчанию восстановлены.  Редактировать Подвал Галереи Главная страница Изображения По данному адресу ничего не найдено. Воспользуйтесь поиском. Кажется, мы можем и найти то, что вы ищете. Возможно, поиск может помочь. Максимальная высота (пикс) Максимальная ширина (пикс) Месяц: %s Следующие комментарии &rarr; Вперед <span class="meta-nav">&rarr;</span> News Вывод записей из категории Не выбрано Файл не выбран Ничего не найдено. Упс! Эта страница не найдена. Настройки сохранены. Страница %s Страницы:  Запись навигация   Записи навигация Основное меню Готовы опубликовать первую запись? <a href="%1$s">Начать здесь</a>. Удалить По умолчанию Сохранить настройки Результаты поиска: %s Боковая колонка Новое - ТОП - Комменты Логотип сайта Извините, по вашему запросу ничего не найдено. Попробуйте другие ключевые слова. Виджет для боковой колонки Опции темы Заголовок: Обновите версию WordPress для полной поддержки всех функций Загрузить Видео Посмотреть файла Выводит записи из конкретной категории для области ”Главной страницы” Год: %s Спасибо! Ваш комментарий ожидает проверки. Один комментарий %1$s Комментариев От %s Опубликовано %s 