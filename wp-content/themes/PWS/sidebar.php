<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package fabthemes
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-md-4">
<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
    <div class="sidebar-banner"><?php drawAdsPlace(array('id'=>5)) ?></div>
    <div class="sidebar-banner"><?php drawAdsPlace(array('id'=>6)) ?></div>
	<?php get_template_part( 'sponsors' ); ?>
</div><!-- #secondary -->
</div>
