<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package fabthemes
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php global $wpTitle; if (isset($wpTitle)) {echo $wpTitle;} else wp_title( '|', true, 'right' ); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php global $wpDescription;
if (empty($wpDescription))
    $wpDescription = 'Свой бизнес - это новости малых предприятий: открытие новых кафе, ресторанов и магазинов, развитие рынка недвижимости, инновационный бизнес, успешный опыт и новые идеи.'; ?>
<meta name="description" content="<?php echo $wpDescription ?>"/>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if (!isset($_SERVER['_IS_DEVELOPER_MODE']) || empty($_SERVER['_IS_DEVELOPER_MODE'])): ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-62260867-1', 'auto');
ga('send', 'pageview');
</script>
<?php endif ?>
<?php wp_head(); ?>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
<script type="text/javascript">
    VK.init({apiId: 4955107, onlyWidgets: true});
</script>
<script type="text/javascript" src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/highslide/highslide-with-gallery.packed.js"></script>
<script type="text/javascript">
hs.graphicsDir = '<?php bloginfo('stylesheet_directory') ?>/js/highslide/graphics/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.outlineType = 'rounded-white';
hs.fadeInOut = true;
hs.showCredits = false;

// Add the controlbar
hs.addSlideshow({
    //slideshowGroup: 'group1',
    interval: 5000,
    repeat: false,
    useControls: true,
    fixedControls: 'fit',
    overlayOptions: {
        opacity: 0.75,
        position: 'bottom center',
        hideOnMouseOut: true
    }
});
</script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/js/highslide/highslide.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/jimmy.css"/>
</head>

<body <?php body_class(); ?>>
<?php if (!isset($_SERVER['_IS_DEVELOPER_MODE']) || empty($_SERVER['_IS_DEVELOPER_MODE'])): ?>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30046504 = new Ya.Metrika({id:30046504, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/30046504" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<?php endif ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="body-wrapper">
<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div class="container"> <div class="row">

			<div class="col-md-5">
				<div class="site-branding">

	<?php if (get_theme_mod(FT_scope::tool()->optionsName . '_logo', '') != '') { ?>
				<h1 class="site-title logo"><a class="mylogo" rel="home" href="<?php bloginfo('siteurl');?>/" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img relWidth="<?php echo intval(get_theme_mod(FT_scope::tool()->optionsName . '_maxWidth', 0)); ?>" relHeight="<?php echo intval(get_theme_mod(FT_scope::tool()->optionsName . '_maxHeight', 0)); ?>" id="ft_logo" src="<?php echo get_theme_mod(FT_scope::tool()->optionsName . '_logo', ''); ?>" alt="" /></a></h1>
	<?php } else { ?>
				<h1 class="site-title logo"><a id="blogname" rel="home" href="<?php bloginfo('siteurl');?>/" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                        <img style="width: 296px" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/><br/>
                        малый бизнес в санкт-петербурге и ленобласти</a></h1>
	<?php } ?>
                    <?php // Informer (currency)
                    /**
                     * Соединение с базой сайта Петростройбаза
                     */
                    if (!isset($db2) || !is_object($db2)) {
                        $db2 = new wpdb('psbuser','JwJbWsdQ','psbaza','localhost');
                    }
                    $eur = $db2->get_row("SELECT `si_value`, `si_date` FROM `site_informers` WHERE `si_date` <= CURDATE() AND `si_type` = 'eur' ORDER BY `si_date` DESC LIMIT 1");
                    $usd = $db2->get_row("SELECT `si_value`, `si_date` FROM `site_informers` WHERE `si_date` <= CURDATE() AND `si_type` = 'usd' ORDER BY `si_date` DESC LIMIT 1");
                    $wMin = $db2->get_row("SELECT `si_value` FROM `site_informers` WHERE `si_date` <= CURDATE() AND `si_type` = 'w_min' ORDER BY `si_date` DESC LIMIT 1");
                    $wMax = $db2->get_row("SELECT `si_value` FROM `site_informers` WHERE `si_date` <= CURDATE() AND `si_type` = 'w_max' ORDER BY `si_date` DESC LIMIT 1");
                    $date = date('l, j F', strtotime($usd->si_date));
//                    $date = date('l, j F');

                    $dayPatterns = array();
                    $dayPatterns[] = '/Monday/';
                    $dayPatterns[] = '/Tuesday/';
                    $dayPatterns[] = '/Wednesday/';
                    $dayPatterns[] = '/Thursday/';
                    $dayPatterns[] = '/Friday/';
                    $dayPatterns[] = '/Saturday/';
                    $dayPatterns[] = '/Sunday/';

                    $dayReplacements = array();
                    $dayReplacements[] = 'Понедельник';
                    $dayReplacements[] = 'Вторник';
                    $dayReplacements[] = 'Среда';
                    $dayReplacements[] = 'Четверг';
                    $dayReplacements[] = 'Пятница';
                    $dayReplacements[] = 'Суббота';
                    $dayReplacements[] = 'Воскресенье';

                    $monthPatterns = array();
                    $monthPatterns[] = '/January/';
                    $monthPatterns[] = '/February/';
                    $monthPatterns[] = '/March/';
                    $monthPatterns[] = '/April/';
                    $monthPatterns[] = '/May/';
                    $monthPatterns[] = '/June/';
                    $monthPatterns[] = '/July/';
                    $monthPatterns[] = '/August/';
                    $monthPatterns[] = '/September/';
                    $monthPatterns[] = '/October/';
                    $monthPatterns[] = '/November/';
                    $monthPatterns[] = '/December/';

                    $monthReplacements = array();
                    $monthReplacements[] = 'января';
                    $monthReplacements[] = 'февраля';
                    $monthReplacements[] = 'марта';
                    $monthReplacements[] = 'апреля';
                    $monthReplacements[] = 'мая';
                    $monthReplacements[] = 'июня';
                    $monthReplacements[] = 'июля';
                    $monthReplacements[] = 'августа';
                    $monthReplacements[] = 'сентября';
                    $monthReplacements[] = 'октября';
                    $monthReplacements[] = 'ноября';
                    $monthReplacements[] = 'декабря';

                    $date = preg_replace($dayPatterns, $dayReplacements, $date);
                    $date = preg_replace($monthPatterns, $monthReplacements, $date);
                    ?>
                <div class="informer"><div><?php echo $date ?></div><?php if (is_object($usd)) { ?><div>&dollar;<?php echo $usd->si_value ?></div><div>&euro;<?php echo $eur->si_value ?></div><?php } ?><?php if (is_object($wMin) && is_object($wMax)){ ?><div>Погода: <?php echo ($wMin->si_value > 0 ? '+' : ''),(int) $wMin->si_value ?>..<?php echo ($wMax->si_value > 0 ? '+' : ''),(int) $wMax->si_value ?></div><?php }?></div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="search-right">
                    <div class="top-banner"><?php drawAdsPlace(array('id'=>4)) ?></div>

					<?php //get_search_form(); ?>
				</div>
			</div>
			<div class="clear"></div>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ,'menu_id' =>'endolf') ); ?>
				</nav><!-- #site-navigation -->
		</div></div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="container"> <div class="row">
