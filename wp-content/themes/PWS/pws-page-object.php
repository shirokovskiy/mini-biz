<?php /* Template Name: PWS-Object-Page */ ?>
<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 04.03.15
 * Time         : 23:13
 * Description  : Шаблон страницы инфо об объекте
 */
$wpTitle = 'Объект недвижимости';
if (isset($wp_query->query_vars['jObject']) && !empty($wp_query->query_vars['jObject'])):
    $sUrl = $wp_query->query_vars['jObject'];

    /**
     * Соединение с базой сайта Петростройбаза
     */
    $db2 = new wpdb('psbuser','JwJbWsdQ','psbaza','localhost');

    /**
     * Выборка объекта недвижимости по URL
     */
    $sql = "SELECT tSeo.*, tSi.si_filename, tSra.sra_name, tSet.set_name, tSr.sr_name, tVid.sev_name"
        . " FROM site_estate_objects tSeo"
        . " LEFT JOIN site_images tSi ON (si_id_rel = seo_id)"
        . " LEFT JOIN site_estate_types tSet ON (seo_id_etype = set_id)"
        . " LEFT JOIN site_estate_vid tVid ON (seo_id_vid = sev_id)"
        . " LEFT JOIN site_regions_adm tSra ON (seo_id_adm_reg = sra_id)"
        . " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)"
        . " LEFT JOIN site_regions tSr ON (sorl_sr_id = sr_id)"
        . " WHERE seo_status = 'Y' AND seo_sb = 'Y' AND seo_url LIKE '$sUrl'"
        . " LIMIT 1";
    $record = $db2->get_row($sql);
    if (is_object($record)) {
        $wpDescription = $wpTitle.' на сайте Свой Бизнес';
        $wpTitle = $record->seo_name;
        $wpDescription .= ': '.$wpTitle.'. '.$record->seo_address.', '.$record->seo_other_etap;
    }
endif;

$wpTitle .= ' | Свой Бизнес';

get_header(); ?>
<div class="col-md-12">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            // выборка инфо по объекту
            if (is_object($record)):
                if (isset($record->seo_id) && $record->seo_id > 0) { ?>
                    <h1 class="block-title"><span><?=$record->seo_name?></span></h1>
                    <?php // Картинки и карта
                    $isMainPhoto = false;
                    $sMainPhoto = $sMainThumb = null;
                    $isAdditionalPhotos = false;
                    $iWidth = 315;
                    $sMainPhoto = 'objects/object.photo.'.$record->seo_id.'.b.jpg';
                    $sMainThumb = 'objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg';

                    if (file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg')) {
                        $isMainPhoto = true;
                    }

                    $sql = "SELECT si_id, si_filename, si_id_rel, si_title FROM `site_images` WHERE `si_foto_object_id` = ".$record->seo_id." AND `si_status` = 'Y' AND `si_type` = 'fotobank'";
                    $fotobankImages = $db2->get_results($sql);

                    if (is_array($fotobankImages) && !empty($fotobankImages)) {
                        foreach($fotobankImages as $oK => $oImage) {
                            if (!$isMainPhoto) {
                                if (!file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg')
                                    && file_exists(THEME_ABSPATH.'/images/groups/'.$oImage->si_id_rel.'/'.$oImage->si_filename)) {

                                    $sMainPhoto = 'groups/'.$oImage->si_id_rel.'/'.$oImage->si_filename;
                                    $sMainThumb = 'groups/'.$oImage->si_id_rel.'/s_m_a_l_l_'.$oImage->si_filename;
                                    $isMainPhoto = true;
                                }

                                if ($isMainPhoto) {
                                    unset($fotobankImages[$oK]);
                                    break;
                                }
                            }
                        }

                        if (count($fotobankImages) > 1)
                            $isAdditionalPhotos = true;
                    } elseif (!file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.mb.w'.$iWidth.'.jpg')
                        && file_exists(THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.b.jpg')) {

                        $image = wp_get_image_editor( THEME_ABSPATH.'/images/objects/object.photo.'.$record->seo_id.'.b.jpg' );
                        if ( ! is_wp_error( $image ) ) {
                            //$image->rotate( 90 );
                            $image->resize( 315, 300, false /* crop */ );
                            $image->save( THEME_ABSPATH.'/images/'.$sMainThumb );
                            $isMainPhoto = true;
                        }
                    }
                    ?>
                    <div class="col-md-4 object-photos">
                        <?php if ($isMainPhoto): ?>
                        <div id="main-photo"><a class="highslide" onclick="return hs.expand(this)" href="<?php bloginfo('stylesheet_directory') ?>/images/<?=$sMainPhoto?>"><img src="<?php bloginfo('stylesheet_directory') ?>/images/<?=$sMainThumb?>" alt="<?=htmlspecialchars($record->seo_name)?>" title="<?=htmlspecialchars($record->seo_name)?>"/></a></div>
                        <?php endif ?>
                        <?php if ($isAdditionalPhotos): ?>
                        <div id="additional-photos">
                            <?php foreach($fotobankImages as $oImage): ?>
                                <div><a class="highslide" onclick="return hs.expand(this)" href="<?php bloginfo('stylesheet_directory') ?>/images/groups/<?=$oImage->si_id_rel?>/<?=$oImage->si_filename?>"><img src="<?php bloginfo('stylesheet_directory') ?>/images/groups/<?=$oImage->si_id_rel?>/t_h_u_m_b_<?=$oImage->si_filename?>" alt="<?=htmlspecialchars($record->seo_name)?>"/></a></div>
                            <?php endforeach ?>
                        </div>
                        <?php endif ?>
                        <div id="map"></div>
                        <div style="clear: both"></div>
                    </div>
                    <?php // Подробная инфо об объекте ?>
                    <div class="col-md-8 object-info">
                        <div>
                            <div class="col-md-4">ID</div>
                            <div class="col-md-8"><?=$record->seo_id?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Назначение объекта</div>
                            <div class="col-md-8"><?=$record->seo_naznach?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Регион</div>
                            <div class="col-md-8"><?=$record->sr_name?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Район</div>
                            <div class="col-md-8"><?= !empty($record->seo_other_adm_reg) ? $record->seo_other_adm_reg : $record->sra_name ?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Адрес</div>
                            <div class="col-md-8"><?=$record->seo_address?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Вид строительства</div>
                            <div class="col-md-8"><?=$record->sev_name?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Этап</div>
                            <div class="col-md-8"><?=$record->seo_other_etap?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Дата этапа</div>
                            <div class="col-md-8"><?=(!empty($record->seo_etap_date) ? date('d.m.Y', strtotime($record->seo_etap_date)) : '')?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Площадь земельного участка</div>
                            <div class="col-md-8"><?=$record->seo_sq_zem_uch?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Площадь застройки</div>
                            <div class="col-md-8"><?=$record->seo_sq_zastr?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Общая площадь</div>
                            <div class="col-md-8"><?=$record->seo_sq_total?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Этажность</div>
                            <div class="col-md-8"><?=$record->seo_etazh?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Сроки проектирования</div>
                            <div class="col-md-8"><?=$record->seo_period_prj?></div>
                        </div>
                        <div>
                            <div class="col-md-4">Сроки строительства</div>
                            <div class="col-md-8"><?=$record->seo_period_bld?></div>
                        </div>

                        <div>
                            <div class="col-md-4">Строительный объём</div>
                            <div class="col-md-8"><?=$record->seo_bld_volume?></div>
                        </div>
                        <?php
                        /**
                         * Контактные данные объекта
                         */
                        $sql = "SELECT * FROM site_estate_contacts"
                            . " LEFT JOIN site_objects_contacts_lnk ON (socl_id_contact = sec_id)"
                            . " WHERE socl_relation = 'zak' AND socl_id_object = $record->seo_id AND sec_status = 'Y'";
                        $arrContactRecords = $db2->get_results( $sql );

                        $arrTitleExt['zak'] = "Заказчик";
                        $arrTitleExt['zas'] = "Застройщик";
                        $arrTitleExt['inv'] = "Инвестор";
                        $arrTitleExt['genpod'] = "Генподрядчик";
                        $arrTitleExt['proekt'] = "Проектировщик";
                        $arrTitleExt['pod'] = "Подрядчик";
                        $arrTitleExt['opa'] = "Отдел продаж и аренды";

                        if ( is_array($arrContactRecords) ) {
                            foreach ( $arrContactRecords as $key => $value ) { ?>
                        <div>
                            <div class="col-md-4"><?=$arrTitleExt[$value->socl_relation]?></div>
                            <div class="col-md-8">
                                <div class="sub-info">
                                    <div class="col-md-3">Компания</div>
                                    <div class="col-md-9"><?=nl2br($value->sec_company)?></div>
                                </div>
                                <div class="sub-info">
                                    <div class="col-md-3">Контакт</div>
                                    <div class="col-md-9"><?=nl2br($value->sec_fio)?></div>
                                </div>
                                <div class="sub-info">
                                    <div class="col-md-3">Должность</div>
                                    <div class="col-md-9"><?=nl2br($value->sec_proff)?></div>
                                </div>
                                <div class="sub-info">
                                    <div class="col-md-3">Доп.инфо</div>
                                    <div class="col-md-9"><?=nl2br($value->sec_contact)?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                    <div style="clear: both"></div>
                    <div class="col-md-4">
                        <a href="/kommercheskaya-nedvizhimost/search/">&laquo; назад к результатам поиска</a>
                    </div>
                    <div class="col-md-8">
                        <div class="fb-like" data-href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-width="315" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

                        <!-- Put this div tag to the place, where the Like block will be -->
                        <div id="vk_like"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like", {type: "button"});
                        </script>
                    </div>
                    <script type="text/javascript">
                        function showEstateObjectMap(myMap, address) {
                            // Поиск координат центра адреса.
                            ymaps.geocode(address, {
                                /**
                                 * Опции запроса
                                 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                                 */
                                boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                                results: 1 // Если нужен только один результат, экономим трафик пользователей
                            }).then(function (res) {
                                // Выбираем первый результат геокодирования.
                                var firstGeoObject = res.geoObjects.get(0);

                                // Область видимости геообъекта.
                                var bounds = firstGeoObject.properties.get('boundedBy');

                                // Добавляем первый найденный геообъект на карту.
                                myMap.geoObjects.add(firstGeoObject);
                                // Масштабируем карту на область видимости геообъекта.
                                myMap.setBounds(bounds, {
                                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                                });

                                /**
                                 * Все данные в виде javascript-объекта.
                                 */
                                // console.log('Все данные геообъекта: ', firstGeoObject.properties.getAll());
                                /**
                                 * Метаданные запроса и ответа геокодера.
                                 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderResponseMetaData.xml
                                 */
                                // console.log('Метаданные ответа геокодера: ', res.metaData);
                                /**
                                 * Метаданные геокодера, возвращаемые для найденного объекта.
                                 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderMetaData.xml
                                 */
                                // console.log('Метаданные геокодера: ', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
                                /**
                                 * Точность ответа (precision) возвращается только для домов.
                                 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/precision.xml
                                 */
                                // console.log('precision', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
                                /**
                                 * Тип найденного объекта (kind).
                                 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
                                 */
                                // console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
                                // console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
                                // console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
                                // console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));

                                /**
                                 * Если нужно добавить по найденным геокодером координатам метку со своими стилями
                                 * и контентом балуна, то создаем новую метку по координатам найденной
                                 * и добавляем ее на карту вместо найденной.
                                 */
                                // Координаты геообъекта.
                                var coords = firstGeoObject.geometry.getCoordinates();

                                var myPlacemark = new ymaps.Placemark(coords, {
                                                iconContent: 'моя метка',
                                                balloonContent: 'Содержимое балуна <strong>моей метки</strong>'
                                            }, {
                                                preset: 'islands#violetStretchyIcon'
                                });

                                myMap.geoObjects.add(myPlacemark);
                            });
                        }


                        jQuery(document).ready(function($){
                            /**
                             * Yandex.Maps
                             */
                            ymaps.ready(function () {
                                var myMap = new ymaps.Map("map", {
                                        center: [60.00, 30.00],// Saint-Petersburg
                                        zoom: 15
                                    });

                                <?php if (!empty($record->seo_coords)): ?>
                                // Создаем геообъект с типом геометрии "Точка".
                                var myGeoObject = new ymaps.GeoObject({
                                        // Описание геометрии.
                                        geometry: {
                                            type: "Point",
                                            coordinates: [<?=$record->seo_coords?>]
                                        },
                                        // Свойства.
                                        properties: {
                                            // Контент метки.
                                            iconContent: '',
                                            hintContent: '<?= $record->seo_name ?>'
                                        }
                                    }, {
                                        // Опции.
                                        // Иконка метки будет растягиваться под размер ее содержимого.
                                        //preset: 'islands#blackStretchyIcon',
                                        preset: 'islands#icon',
                                        // Метку можно перемещать.
                                        draggable: false
                                });

                                myMap.geoObjects.add(myGeoObject);
                                myMap.setCenter([<?=$record->seo_coords?>]);
                                <?php elseif(!empty($record->seo_address)): ?>
                                showEstateObjectMap(myMap, '<?=htmlspecialchars($record->seo_address)?>');
                                <?php endif ?>
                            });
                        });
                    </script>
            <?php
                } else { ?>
                    <h1 class="block-title"><span>КОММЕРЧЕСКАЯ НЕДВИЖИМОСТЬ НА СТАДИИ СТРОИТЕЛЬСТВА И РЕМОНТА</span></h1>
                    <div>
                        Ошибка. Объект не найден.
                    </div>
            <?php
                } ?>
<?php       endif; ?>
        </main><!-- #main -->
    </div><!-- #primary -->
</div>
<?php get_footer(); ?>
