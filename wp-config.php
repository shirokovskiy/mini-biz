<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'mini_biz');

/** Имя пользователя MySQL */
define('DB_USER', 'minibiz');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'RyBtu7mZrcfGLy28');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b(h[$R5l`dmF;HsjsPpy& r>Bl4g@+9t:U^{PW<1emwF%Pu,s$]v+L_H[Kb0.[M{');
define('SECURE_AUTH_KEY',  '+0+KA<S`Kw#%RUnpC~F|WaR#bEF01a|m_`7I=R1a=A`q+5gck<vO)rG<0(bpL^:q');
define('LOGGED_IN_KEY',    '8gGR5Z<-31Z/U|?BZ%6qA`G1H!#MHh@+}*JVerGt%5-bBga-*P?zH})>`~F;HGY2');
define('NONCE_KEY',        '/r%4 b6AJhNZ3lr=y9^3.yv;:1=K):]-h|?1WNKe9O%JId?(|VZ_~ds1nPH=OU|o');
define('AUTH_SALT',        '5qn=_k?,2)LxE T;KpV}:dV-6S[jE:GnwQl8Yu{<YJKSUrhSXZ?<rH-k84@b9P{!');
define('SECURE_AUTH_SALT', '<kY5Qq|rnoZmu5=#(LfBq1_$RKf{$)f42N-*<8P<AXDh`#>kuw45(Tsy,M5%7oL<');
define('LOGGED_IN_SALT',   't U_|{VMl0/SJb$7%$nY6+R;jGg9vW&= |^L+Wg|CREYL)Ld[WX:D/X|0bbP,dG?');
define('NONCE_SALT',       'jvjEDb:V)m]LuMdZG{T+`CF~P=eQcAFWo^wt`N9Eqz}eyea&v{A`PKY|{w,VJ)`=');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
