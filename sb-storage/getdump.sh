#!/bin/bash

# Remote Server Data
DBNAME=mini_biz
SSHAUTH=psbaza

SOURCE="${BASH_SOURCE[0]}"
SQL_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $SQL_DIR
SQL_FILE=$SQL_DIR/$DBNAME.sql
BZIP_FILE=$SQL_FILE.bz2

if [ -f $BZIP_FILE ];
then
	echo "Delete previous bz2 file if exist..."
	rm -f $BZIP_FILE
fi

if [ -f $SQL_FILE ];
then
	echo "Backup previous SQL file if exist..."
	BKP_FILE=$SQL_FILE.$(date +%Y-%m-%d_%H.%M).bkp.sql
	mv -f $SQL_FILE $BKP_FILE
	bzip2 $BKP_FILE
fi

echo "Download MySQL Database: $DBNAME"
#ssh $SSHAUTH "mysqldump --defaults-extra-file=minibiz.cnf --where='1 limit 3000' --opt $DBNAME | bzip2 -9" > $BZIP_FILE
ssh $SSHAUTH "mysqldump --defaults-extra-file=minibiz.cnf --opt $DBNAME | bzip2 -9" > $BZIP_FILE
if [ -f $BZIP_FILE ];
then
    echo "Unzip file"
    bunzip2 $BZIP_FILE
    echo "Restore database locally."
    mysql --defaults-extra-file=mysql.cnf -f $DBNAME < $SQL_FILE
    echo "Re-configure database for local usage."
    mysql --defaults-extra-file=mysql.cnf -f $DBNAME < $SQL_DIR/re-configure.sql
    rsync -huzavr psbaza:www/mini-biz.ru/wp-content/uploads ../wp-content/
else
    echo "No downloaded archive file: $BZIP_FILE"
fi

echo "Done "$(date "+%Y-%m-%d %H:%M")
